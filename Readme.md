Makes the below typeahead based address picker available in meteor applications.

This package has sergeyt:typeahead and dburles:google-maps as dependencies.

See [href="https://github.com/sgruhier/typeahead-addresspicker"](https://github.com/sgruhier/typeahead-addresspicker)