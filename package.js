Package.describe({
  name: 'votercircle:meteor-typeahead-addresspicker',
  version: '1.0.1',
  summary: 'Wraps the typeahead-addresspicker for use in meteor apps',
  git: 'https://bitbucket.org/votercircle/meteor-typeahead-addresspicker',
  documentation: 'README.md'
});

Package.onUse(function(api) {
    // meteor version
    api.versionsFrom('1.0');

    api.use('sergeyt:typeahead@0.11.1_4');
    api.use('dburles:google-maps@1.1.4');
    api.addFiles('typeahead-addresspicker.js', 'client');
});